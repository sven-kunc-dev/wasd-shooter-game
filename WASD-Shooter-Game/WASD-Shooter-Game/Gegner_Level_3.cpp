#include "stdafx.h"
#include "Gegner_Level_3.h"

#include <stdlib.h> //Standard c und c++ Bibliotheken
#include <conio.h> //Cursor platzieren - Der Name conio kommt von CONsole Input/Output.
#include <stdio.h> // Ein-/Ausgabe 1
#include <iostream>// Ein-/Ausgabe 2
#include <windows.h> //zur Kontrolle �ber das Terminal

Gegner_Level_3::Gegner_Level_3(int _x, int _y)
{
	x = _x;
	y = _y;
	isAlive = true;
}

void Gegner_Level_3::CursorZurKoordinate(int x, int y) {
	//zum "Umherlaufen" innerhalb des Controlterminals
	HANDLE hCon = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD dwPos;
	dwPos.X = x; // startet bei 0
	dwPos.Y = y; // startet bei 0
	SetConsoleCursorPosition(hCon, dwPos);
}


void Gegner_Level_3::Zeichne() {
	CursorZurKoordinate(x, y); printf("3"); // "3" !!
}
void Gegner_Level_3::Loesche()
{
	CursorZurKoordinate(x, y); printf(" "); // "3 weg" !!
}
void Gegner_Level_3::MoveUp()
{ // Hauptbewegungsfunktion f�r den Gegner 3
	CursorZurKoordinate(x, y); printf(" "); // "Gegner weg" !!
	CursorZurKoordinate(x, --y); printf("3"); // "Gegner" 3 !!

}
void Gegner_Level_3::MoveDown()
{ // Hauptbewegungsfunktion f�r den Gegner 3
	CursorZurKoordinate(x, y); printf(" "); // "Gegner weg" !!
	CursorZurKoordinate(x, ++y); printf("3"); // "Gegner" 3 !!

}

