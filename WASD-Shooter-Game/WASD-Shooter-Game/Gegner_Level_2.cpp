#include "stdafx.h"
#include "Gegner_Level_2.h"

#include <stdlib.h> //Standard c und c++ Bibliotheken
#include <conio.h> //Cursor platzieren - Der Name conio kommt von CONsole Input/Output.
#include <stdio.h> // Ein-/Ausgabe 1
#include <iostream>// Ein-/Ausgabe 2
#include <windows.h> //zur Kontrolle �ber das Terminal

Gegner_Level_2::Gegner_Level_2(int _x, int _y)
{
	x = _x;
	y = _y;
}

void Gegner_Level_2::CursorZurKoordinate(int x, int y) {
	//zum "Umherlaufen" innerhalb des Controlterminals
	HANDLE hCon = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD dwPos;
	dwPos.X = x; // startet bei 0
	dwPos.Y = y; // startet bei 0
	SetConsoleCursorPosition(hCon, dwPos);
}


void Gegner_Level_2::Zeichne() {
	CursorZurKoordinate(x, y); printf("2"); // "Gegner Nr 2" * !!
}
void Gegner_Level_2::Loesche()
{
	CursorZurKoordinate(x, y); printf(" "); // "Nr 2 weg" !!
}
void Gegner_Level_2::MoveLeft()
{ // Hauptbewegungsfunktion f�r den Gegner 2
	CursorZurKoordinate(x, y); printf(" "); // "Gegner weg" !!
	CursorZurKoordinate(--x, y); printf("2"); // "Gegner" 2 !!

}
void Gegner_Level_2::MoveRight()
{ // Hauptbewegungsfunktion f�r den Gegner 2
	CursorZurKoordinate(x, y); printf(" "); // "Gegner weg" !!
	CursorZurKoordinate(++x, y); printf("2"); // "Gegner" 2 !!

}


