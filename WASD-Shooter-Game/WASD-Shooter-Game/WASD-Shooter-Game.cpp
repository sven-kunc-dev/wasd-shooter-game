#include "stdafx.h" //Standardinclude f�r MS Visual Studio 2015
/*
* WASD-Shooter-Game - ein Projekt im FWPF-Modul C++ f�r C#-Entwickler
*                     der TH Georg Simon Ohm N�rnberg - WS19/20
*					  betreut von Prof. Rymon von Lipinski
*					  entwickelt durch Eugen Antonenko und Sven Kunc
*
*/
//Inkludes f�r die Programmlogik
#include <stdlib.h>		//Standard c und c++ Bibliotheken
#include <conio.h>		//Cursor platzieren - Der Name conio kommt von CONsole Input/Output.
#include <stdio.h>		// Ein-/Ausgabe 1
#include <iostream>		// Ein-/Ausgabe 2
#include <windows.h>	//zur Kontrolle �ber das Terminal
#include <random>		//Zufallszahlen generieren 
#include <typeinfo>		//for 'typeid' to work  
#include <list>			//Liste f�r dynamische Speicherverwaltung
#include <exception>	//Ausnahmebehandlung (Sven)

//Inkludes f�r die Spiellogik
#include "Gegner_Level_1.h"
#include "Gegner_Level_2.h"
#include "Gegner_Level_3.h"
#include "Waldbeere.h"
#include "Powerschuss.h"
#include "Hindernis_Level_1.h"
#include "Hindernis_Level_2.h"
#include "MUSTER_Gegner_Powerup_Hindernis.h"

//n�tzliche usings
using namespace std;

// /////////////////////// //////////////////////// ////////////////////// //////////////// //////////////////// ////////////////////////////
/*
Was ist zu wissen? --> C++ Grundwissen siehe Skript und Programmbeispiele aus der Vorlesung

Arbeitsschritte:
Organisatorisches, Aufgabenverteilung, Arbeitsgrundlage (Basis), gemeinsames Brainstorming, Projektdurchf�hrung: siehe unten
/*
ToDo-Log (WICHTIGES OBEN):###2D, Spieler, Waldbeeren, B�ume, "Welt",...###//TODOOODez�19OOOOOOOODez�19/Jan�20O20OOOOOOJan�20OOOOOS!-->
### 1.Init:
- Cursor verstecken									-check Dez�19
- Spielfeldbegrenzung zeichnen						-check Dez�19
- CursoZurKoordinate-Funktion entwickeln			-check Dez�19
- Spielfigurbewegung								-check Dez�19
### 2. Programmablauf
- Spielfigur-Klasse erstellen							- check	Dez�19
- Gegner-LEVEL-1-Klasse erstellen						- check Dez�19 ausgelagert, #Vererbung?
- Gegner-LEVEL-2-Klasse erstellen						- check Jan�20 ausgelagert,			
- Gegner-LEVEL-3-Klasse erstellen						- check Jan�20 ausgelagert, 
- (Power-)Schuss-Klasse erstellen						- check Dez�19/Jan�20
(- Hindernis-Klasse-erstellen							- check Dez�19/Jan�20 -> Spielfeldgrenzen sind Hindernisse)
+BONUS											-
- PowerUp-Klassen: (Spielfigur bekommt F�higkeiten beim Fressen, bei RankUps oder sowas ...)		- check - Schussf�higkeit
- Waldbeeren-Klasse erstellen f�r PowerUps				- check Dez�19/Jan�20

### 3. Kollision (Speicherbereinigung)									
- Bewegung verfolgen (Methode in Spielfigur)			- check Jan�20
- Berechnung/Pr�fung (Ablauf in der main)				- check Jan�20

### 4. Schussklasse erstellen							- check Dez�19/Jan�20

### 5. Gegnerbewegung und Spielablauf vervollst�ndigen	- check Jan�20

### 6. Sprachelement je Teammitglied					- check Jan�20

### X. PROJEKTMANAGEMENT
- Dokumentmanagement:Google Drive (Powerpoint, Kalender..)
- Sourcecodeverwaltung: GitLab
- Kommunikation: Termine und WhatsApp
*/

// ///// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ///// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//GLOBALES zuerst....

// WASD-Starungstasten #ASCII-Nummern, alternativ mit Pfeiltasten 
#define W 119	//UP    72 
#define A 97	//LEFT  75
#define D 100	//RIGHT 77
#define S 115	//DOWN  80
#define SPACE 32//Powerschuss

unsigned counterFuerObjektgeschwindigkeit = 0;
unsigned tmp2Counter = 0;
unsigned tmp3CounterLinks = 0;
unsigned tmp3CounterRechts = 0;

// I - INIT und Spielvorbereitung
void versteckeCursor()
{ // cursorhiding :3
	HANDLE hCon = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO cci;
	cci.dwSize = 1;
	cci.bVisible = FALSE;
	SetConsoleCursorInfo(hCon, &cci);
}
//		Positionierung -------> x/y
void CursorZurKoordinate(int x, int y)
{ //zum "Umherlaufen" innerhalb des Controlterminals
	HANDLE hCon = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD dwPos;
	dwPos.X = x; // startet bei 0
	dwPos.Y = y; // startet bei 0
	SetConsoleCursorPosition(hCon, dwPos);
}
void zerstoereZeichen(int a_x, int a_y, int b_x, int b_y)  //Parameterreihenfolge: linksoben, linksunten, rechtsoben, rechtsunten  
{ // um ein bestimmmtes Zeichen im Terminal zu l�schen
	//Reihenfolge: links nach rechts, oben nach unten - (a nach b, x nach y)
	for (int i = a_x; i < b_x; i++)
	{
		for (int j = a_y; j < b_y; j++)
		{
			CursorZurKoordinate(i, j); printf(" ");
		}
	}
}
//		Spielfeld
/// /////////////////
/*
	  ax(6/6) ----------bx(99/6)
		|					 |
		|					 |
      ay(6/25)----------by(99/25)
*/
const int xMin = 6, yMin = 6;
const int xMax = 99, yMax = 24;
///
void zeichneFensterrahmen(int a_x, int a_y, int b_x, int b_y)
{ // zum Zeichnen mittels Geraden: 
	//rechtige Fensterrahmen durch 4x2 definierte Punkte + 4 Ecken
	zerstoereZeichen(a_x, a_y, b_x, b_y);
	for (int i = a_x; i < b_x; i++)
	{
		CursorZurKoordinate(i, a_y); printf("%c", 205); //OBEN "="-char
		CursorZurKoordinate(i, b_y); printf("%c", 205); //UNTEN "="-char
	}
	for (int i = a_y; i < b_y; i++)
	{
		CursorZurKoordinate(a_x, i); printf("%c", 186); //LINKS "||"-char
		CursorZurKoordinate(b_x, i); printf("%c", 186); //RECHTS "||"-char
	}
	CursorZurKoordinate(a_x, a_y); printf("%c", 201); //"linksoben-Ecke"-char
	CursorZurKoordinate(b_x, a_y); printf("%c", 187); //"rechtsoben-Ecke"-char
	CursorZurKoordinate(a_x, b_y); printf("%c", 200); //"linksunten-Ecke"-char
	CursorZurKoordinate(b_x, b_y); printf("%c", 188); //"rechtsunten-Ecke"-char
}
void zeichneSpielfenstergrenzen()
{ // zum Zeichnen von den Spielfigurangaben (siehe unten) und Fensterrahmen
	zeichneFensterrahmen(4, 5, 100, 25); // ###The default size of the Windows terminal is 25 rows x 80 columns
	CursorZurKoordinate(30, 1); printf("WASD-Shooter-Game mit C++!");
	CursorZurKoordinate(1, 2); printf("----------------------------------------------------------------------------------------------------");
	CursorZurKoordinate(5, 4);  printf("LIFE:");
	CursorZurKoordinate(25, 4); printf("POWER:");
	CursorZurKoordinate(50, 4); printf("PUNKTESPIELSTAND:");
	CursorZurKoordinate(1, 27); printf("----------------------------------------------------------------------------------------------------");
	CursorZurKoordinate(10, 28); printf("made by Eugen Antonenko und Sven Kunc - TH Nuernberg Georg Simon Ohm - WS19/20");
}
void fuelleGesamtesSpielfeld(char *c) {
	//Spielfeld befuellen: mit beliebigem char per Zeiger darauf #Symboltabelle
	for (int x = 6; x < 99; x++)
	{
		for (int y = 6; y < 25; y++)
		{
			CursorZurKoordinate(x, y);  printf(c);
		}
	}
}
//Standard Nachrichten die zum Spiel geh�ren:
void spielbeginnNachricht(bool & spielLaueft) {
	fuelleGesamtesSpielfeld("*");
	zeichneFensterrahmen(35, 11, 60, 17);
	CursorZurKoordinate(40, 14); printf("START MIT ENTER");
	while (cin.get() != '\n') //ENTER
	{ //bei Tastenanschlag - "key board hit"
		spielLaueft = true;
	}
}//ToDo
void spielGewonnenNachricht() {
	// bei Spielsieg wird diese Nachricht angezeigt
	fuelleGesamtesSpielfeld("*");
	zeichneFensterrahmen(35, 11, 60, 17);
	CursorZurKoordinate(38, 14); printf("SPIEL GEWONNEN!!! :)");
}
void spielvorbeiNachricht() {
	// bei Game Over wird diese Nachricht angezeigt
	fuelleGesamtesSpielfeld("*");
	zeichneFensterrahmen(40, 11, 55, 17);
	CursorZurKoordinate(43, 14); printf("Game Over");
} 
///////other stuff...//////////
int meinZufallszahlengenerator(int rangeFrom, int rangeTo) {
	//ZUFALLSZAHLEN GENERIEREN 
	std::random_device rd; // obtain a random number from hardware
	std::mt19937 eng(rd()); // seed the generator
	std::uniform_int_distribution<> distr(rangeFrom, rangeTo); // DEFINE THE RANGE
															   /*
															   for (int n = 0; n<40; ++n) {
															   std::cout << distr(eng) << ' ' << std::endl; // generate numbers
															   }*/
	return distr(eng);
}


//Beginn Dynamische Speicherobjektverwaltung Deklaration
// ///// -----------------------------------------------
//Waldbeeren
list<Waldbeere*> Waldbeeren; // Dynamische Liste zur Speicherverwaltung der Waldbeeren
list<Waldbeere*>::iterator it_wb; // mit Iterator
list<Waldbeere*> WaldbeerenCreator(int anzahl) {
	//erstellt eine Liste an 
	list<Waldbeere*> Waldbeeren;

	int xRandom, yRandom;

	//15 Waldbeeren verteilen
	for (int i = 0; i < anzahl; i++)
	{
		//zuf�llige Beeren
		xRandom = meinZufallszahlengenerator(xMin, xMax);
		yRandom = meinZufallszahlengenerator(yMin, yMax);

		Waldbeeren.push_back(new Waldbeere(xRandom, yRandom));
	}
	return Waldbeeren;
}
//Gegner
list<Gegner_Level_1*> Gegner_1; // Dynamische Liste zur Speicherverwaltung des Gegner_Level_1
list<Gegner_Level_1*>::iterator it_g1; // mit Iterator
list<Gegner_Level_2*> Gegner_2; // Dynamische Liste zur Speicherverwaltung des Gegner_Level_2 
list<Gegner_Level_2*>::iterator it_g2; // mit Iterator
list<Gegner_Level_3*> Gegner_3links; // Dynamische Liste zur Speicherverwaltung des Gegner_Level_3
list<Gegner_Level_3*>::iterator it_g3links; // mit Iterator
list<Gegner_Level_3*> Gegner_3rechts; // Dynamische Liste zur Speicherverwaltung des Gegner_Level_3
list<Gegner_Level_3*>::iterator it_g3rechts; // mit Iterator

//Sch�sse des Gegners Nr. 2
list<Powerschuss*> Gegner2Schuesse;// Dynamische Liste zur Speicherverwaltung der Powersch�sse
list<Powerschuss*>::iterator it_gs; // mit Iterator
									   
//Sch�sse der Spielfigur
list<Powerschuss*> Powerschuesse;// Dynamische Liste zur Speicherverwaltung der Powersch�sse
list<Powerschuss*>::iterator it_ps; // mit Iterator

//Hindernisse ... siehe Spielfeldgrenze ;)


// ///// -----------------------------------------------
//Ende Dynamische Speicherobjektverwaltung Deklaration



//Beginn Klasse Spielfigur
class Spielfigur
{

private:
	int x; // x Koordinate
	int y; // y Koordinate
	int life; // Lebenspunkte
	int power; // Powerpunkte
	int punktespielstand; //Highscorepunkte
	bool player1isDead; // lebt die Spielfigur?!
public:
	int X() { return x; }
	int Y() { return y; }
	int HP() { return life; }
	int Power() { return power; }
	int Punktespielstand() { return punktespielstand; }
	void setzeHP(int _life) { life = _life; }
	void inkrHP() { if (life < 5) life += 1; }
	void setzePower(int _power) { power = _power; }
	void inkrPower() { if(power < 5) power += 1; }
	void setzePunktespielstand(int _punktespielstand) { punktespielstand = _punktespielstand; }
	void setzePlayer1Dead (bool _player1isDead) { player1isDead = _player1isDead; }
	bool isDead()
	{
		zeichneSpielstandsanzeige(); //Update der Spielfigurinformation
		return player1isDead;
	}

	//Konstruktor
	Spielfigur(int _x, int _y, int _life, int _power)
	{
		x = _x;
		y = _y;
		life = _life; // Leben
		power = _power; // Powerpunkte
		punktespielstand = 0;
		player1isDead = false; // mit Leben ist die Spielfigur nicht tot
	}
	void zeichneSpielstandsanzeige()
	{ // gibt Lebens- und Poweranzeige wieder (siehe Spielfenstergrenzen())		
	  
		//Leben
		CursorZurKoordinate(12, 4); printf("     ");
		for (int i = 0; i < life; i++)
		{
			CursorZurKoordinate(12 + i, 4); printf("%c", 184);
		}
		//Power
		CursorZurKoordinate(33, 4); printf("     ");		
		for (int i = 0; i < power; i++)
		{
			CursorZurKoordinate(33 + i, 4); printf("%c", 221);
		}
		//Punktescore
		CursorZurKoordinate(70, 4); printf("    ");		
		CursorZurKoordinate(70, 4); cout<<(punktespielstand);
		
	}
	void zeichneSpielfigur()
	{ // Festlegen der Gr��e und Form der Spielfigur
		//zuKoordinate(x, y);     printf("  %c  ", 30);
		CursorZurKoordinate(x, y + 1); printf("  %c  ", 220);
		CursorZurKoordinate(x, y + 2); printf("%c%c%c%c%c", 15, 30, 223, 30, 15);
	}
	void loescheSpielfigur()
	{ // L�schen und Kl�ren der Zeichen, bspw.: alte Position, neuer Zustand, ..
		//zuKoordinate(x, y);     printf("     ");
		CursorZurKoordinate(x, y + 1); printf("     ");
		CursorZurKoordinate(x, y + 2); printf("     ");
	}
	void zerstoereSpielfigur()
	{
		CursorZurKoordinate(this->x, this->y + 1);  printf("  #  ");
		CursorZurKoordinate(this->x, this->y + 2);  printf("#####");
		Sleep(125);
		CursorZurKoordinate(this->x, this->y + 1);  printf("  �  ");
		CursorZurKoordinate(this->x, this->y + 2);  printf("�����");
		Sleep(125);
		CursorZurKoordinate(this->x, this->y + 1);  printf("  #  ");
		CursorZurKoordinate(this->x, this->y + 2);  printf("#####");
		Sleep(125);
		CursorZurKoordinate(this->x, this->y + 1);  printf("    ");
		CursorZurKoordinate(this->x, this->y + 2);  printf("    ");
	}
	//Bewegung
	void Move()
	{ // Hauptbewegungsfunktion f�r die Spielfigur
		if (_kbhit())
		{ //bei Tastenanschlag - "key board hit"

			loescheSpielfigur(); // 1-Zuallererst: alte Position der Spielfigur loeschen

			//2- Eingabe des Users - "Merke den letzten Tastenanschlag";
			char key = _getch(); 
			switch (key) //Belegte Tasten: WASD - 
			{ // �berpruefen der Spielfeldgrenzen durch Fallunterscheidung
			case A:  if (x > 5)      x -= 1; break;
			case D: if (x + 4 < 99) x += 1; break;
			case W:    if (y > 5)      y -= 1; break;
			case S:  if (y + 2 < 24) y += 1; break; 
			case SPACE: 
			if (power <= 5 && power > 0) //max 5 Powershots sonst aufladen mit Waldbeeren!
			 {
				Powerschuesse.push_back(new Powerschuss(x + 2, y + 1));	//ps1 = Powerschuss(x + 2, y - 1);	
				--power;				
				break;

			 }
			}
			zeichneSpielstandsanzeige();
		}
		zeichneSpielfigur(); // Am Ende wird die Spielfigur bewegt: durch Zeichnen der neuen Koordinaten
	}
	//Kollision
	template <typename T> bool pruefeKollision(T *_it)
	{//generische �berpr�fung eines jeden Iteratorelements auf Kollision mit der Spielfigur					 
		auto &it = *_it; //jedes Element, ob Freund oder Feind, statisch zur Laufzeit festlegen und dann auf Kollision �berpr�fen
		if(((it.x >= this->x) && (it.x < this->x + 5)) && (((it.y > this->y) && (it.y <= this->y + 2)))) //Spielfigur "auf" dem Objekt: rechts und links ..UND.. dr�ber und drunter 
		{				
			//f�r alle Typen, au�er Waldbeeren, wird die Spielfigur von einer Zerst�rung ersch�ttert
			if(typeid(*_it) != typeid(Waldbeere)) //
			 this->zerstoereSpielfigur();
			
			return true;			
		}
		else 
			return false;
			
	}	
		
}; //Ende Klasse Spielfigur


//Beginn Pr�sentation Sprachelement Sven: Ausnahmebehandlung
struct SvensException : public std::exception
{
	const char * what() const throw ()
	{
		fuelleGesamtesSpielfeld("S");
		zeichneFensterrahmen(15, 11, 85, 17);;
		CursorZurKoordinate(23, 13); //printf("Exception!");
		return "EXCEPTION: Vielen Dank f�r die Aufmerksamkeit";
	}
} structSvensException;
//Ende Pr�sentation Sprachelement Sven: Ausnahmebehandlung


//.... GLOBALES ENDE

//
///
////

/************************************
/*#.
AB HIER SPIELABLAUF !!!!
AB HIER SPIELABLAUF !!!!	//II - Alles was zum Spielablauf dazugeh�rt ab hier! ------------------------------------------------->
AB HIER SPIELABLAUF !!!!

*/// *********************************

////
///
//

//1a.-Methode f�r die Programmlogik
void Einmalige_Waldbeeren_und_Gegner_erstellen(){
	
	//einmalig beliebig viele Waldbeeren erstellen und dann jede Waldbeere zeichnen; wird eine Waldbeere im sp�teren Spielverlauf gefressen, wird diese der Liste entfernt und eine neue hinzugef�gt
	Waldbeeren = WaldbeerenCreator(75);
	for (it_wb = Waldbeeren.begin(); it_wb != Waldbeeren.end(); it_wb++)
	{
		(*it_wb)->Zeichne();
	}
	//einmalig einen Gegner1 erstellen
	Gegner_1.push_back(new Gegner_Level_1(meinZufallszahlengenerator(xMin, xMax), meinZufallszahlengenerator(yMin, yMax)));
	(*Gegner_1.begin())->Zeichne(); //.begin(), weil der Iterator hier nur einmalig verwendet wird und sowieso nur 1 Element in der Liste existiert

	//einmalig einen bewegeneden und schie�enden Gegner2 erstellen
	Gegner_2.push_back(new Gegner_Level_2(25, 10)); //Startpunkt links oben
	(*Gegner_2.begin())->Zeichne(); //.begin(), weil der Iterator hier nur einmalig verwendet wird und sowieso nur 1 Element in der Liste existiert

	//einmalig zwei bewegenede Gegner3 erstellen
	Gegner_3links.push_back(new Gegner_Level_3(13, 23)); //Startpunkt links unten
	(*Gegner_3links.begin())->Zeichne(); //.begin(), weil der Iterator hier nur einmalig verwendet wird und sowieso nur 1 Element in der Liste existiert
	Gegner_3rechts.push_back(new Gegner_Level_3(91, 7)); //Startpunkt rechts oben
	(*Gegner_3rechts.begin())->Zeichne(); //.begin(), weil der Iterator hier nur einmalig verwendet wird und sowieso nur 1 Element in der Liste existiert

}

/**//**/
int main()
{
	try
	{
		bool spielLaueft = false;
		bool * spielLaueft_ptr;
		spielLaueft_ptr = &spielLaueft;

		//Init
		versteckeCursor();
		spielbeginnNachricht(*spielLaueft_ptr); // ToDo
		*spielLaueft_ptr = &spielLaueft;
		zeichneSpielfenstergrenzen();
		//fuelleGesamtesSpielfeld("#"); //Spielfeld befuellen: mit beliebigem char per Zeiger darauf #Symboltabelle
				 
		//Spielerinstanz
		Spielfigur player1 = Spielfigur(40, 20, 5, 5); // Los geht�s!   # (x/y/life/power)

		//Start-Werte: Punktezahl, Leben bei 0
		int punkteJeRunde = 0;
		int lebenJeRunde = player1.HP();
		int powerJeRunde = player1.Power();

		//...

		//#1a. Zuerst: Zeichnen der Objekte jeder Szene - hier einmalig
		Einmalige_Waldbeeren_und_Gegner_erstellen();
		//ToDo: Andere Objekte erstellen, Random, Zeitintervalle, Gegner mit Powerschuss

		/**/
		/**//**/
		/**//**//**/
		//<----------- SPIELSCHLEIFE
		while (spielLaueft && (!player1.isDead() && player1.Punktespielstand() <= 999999)) //bei HighScore ist Schluss: 1.000.000
		{

			//#1b. Zuerst: Zeichnen der Objekte jeder Szene - hier st�ndig aktualisierend...			
			//
			////#i) ...Powerschuss:
			for (it_ps = Powerschuesse.begin(); it_ps != Powerschuesse.end(); it_ps++)
			{ //Bewegung jede 5. Runde
				if (counterFuerObjektgeschwindigkeit % 5 == 0)
					(*it_ps)->Move();
			}

			////#iia) ...Gegner2schuss-Geschwindigkeit: 
			for (it_gs = Gegner2Schuesse.begin(); it_gs != Gegner2Schuesse.end(); it_gs++)
			{ //Bewegung jede 23. Runde				
				if (counterFuerObjektgeschwindigkeit % 23 == 0)
				{
					(*it_gs)->GegnerMove();
				}
			}

			////#iib) ...Gegner2bewegung Zeichnen 
			if (counterFuerObjektgeschwindigkeit % 50 == 0)
			{ //jede 50. Runde
				if (tmp2Counter < 51)
					(*Gegner_2.begin())->MoveRight();
				else if (tmp2Counter <= 100)
					(*Gegner_2.begin())->MoveLeft();
				else
					tmp2Counter = 0; //nach hin und her wieder auf 0 setzen				
				tmp2Counter++; //jede Runde inkrementieren, nach 100 von vorne				
			}

			////#iii) ....Gegner3bewegungen Zeichnen ...						
			if (((*Gegner_3links.begin())->isAlive))
			{///#iiia)... LINKS - Geschwindigkkeit: jede 32.Runde bewegen: 
				if (counterFuerObjektgeschwindigkeit % 32 == 0)
				{ 
					if (tmp3CounterLinks < 15) {
						(*Gegner_3links.begin())->MoveUp();
					}
					else if (tmp3CounterLinks < 29) {
						(*Gegner_3links.begin())->MoveDown();
					}
					else
						tmp3CounterLinks = 0; //reset
					tmp3CounterLinks++; //jede Runde inkrementieren, nach 100 von vorne
				}
			}
			////#iii) ....Gegner3bewegungen Zeichnen ...						
			if ((((*Gegner_3rechts.begin())->isAlive))) 
			{//#iiib)... RECHT - Geschwindigkkeit: jede 32.Runde bewegen: 
				if (counterFuerObjektgeschwindigkeit % 32 == 0)
				{ 
					if (tmp3CounterRechts < 15) {
						(*Gegner_3rechts.begin())->MoveDown();
					}
					else if (tmp3CounterRechts < 29) {
						(*Gegner_3rechts.begin())->MoveUp();
					}
					else
						tmp3CounterRechts = 0; //reset
					tmp3CounterRechts++; //jede Runde inkrementieren, nach 100 von vorne
				}
			}
			
				
			//#v) ...Gegner2schusserstellung (keine Zeichnung)
			if (counterFuerObjektgeschwindigkeit % 666 == 0) //jede 666.te Runde, ein b�ser Gegner
				Gegner2Schuesse.push_back(new Powerschuss(((*Gegner_2.begin())->x), ((*Gegner_2.begin())->y) + 1));

			//
			counterFuerObjektgeschwindigkeit++; //Spielschleifenz�hler inkrementieren
			//
			
			//######################### #2. PRUEFUNGEN ################################################
			//
			//Pr�fung auf GameOver
			if (player1.HP() == 0 || player1.isDead() || lebenJeRunde <= 0)
			{
				spielLaueft = false;
				player1.setzePlayer1Dead(true);
			}

			//#2a. "VERSCHACHTELTE SCHLEIFEN" - Pr�fungen der Powersch�sse auf Kollision mit Waldbeeren, Gegner1-Treffer, 
			//# ##### ###### ####### #//
			for (it_ps = Powerschuesse.begin(); it_ps != Powerschuesse.end(); it_ps++)
			{  // # BEGINN #f�r jeden Powerschuss auf Waldbeeren-Koordinaten �berpr�fen, zu Beginn der Pr�fung noch keine Kollision
				bool istKollidiert = false;
				for (it_wb = Waldbeeren.begin(); it_wb != Waldbeeren.end(); it_wb++)
				{ 
					// Waldbeeren-Powerschuss-Kollisionspr�fung
					int wbX = (*it_wb)->x; //x-Koordinaten eines jeden Iteratorelements: Waldbeeren
					int wbY = (*it_wb)->y; //y-Koordinaten eines jeden Iteratorelements: Waldbeeren
					int psX = (*it_ps)->x; //x-Koordinaten eines jeden Iteratorelements: Powersch�sse
					int psY = (*it_ps)->y; //y-Koordinaten eines jeden Iteratorelements: Powersch�sse

					if ((wbX == psX) && (wbY == psY))  
					{ //Pr�fe f�r jedes Objekt, ob es vom Powerschuss getroffen wurde
					  
						CursorZurKoordinate(psX, psY); printf(" "); // Powerschuss loeschen
						CursorZurKoordinate(wbX, wbY); printf("X");	 //ExXxplosion
						Sleep(100);									//Sleep zur Visualisierung
						CursorZurKoordinate(wbX, wbY); printf(" ");	// X weg
							//Speicherbereinigung
						delete(*it_ps);						//Zers�rung des Powerschuss-Objekts
						it_ps = Powerschuesse.erase(it_ps);	//L�schung des Powerschuss-Objekts
						delete(*it_wb);						//Zers�rung des Waldbeere-Objekts
						it_wb = Waldbeeren.erase(it_wb);	//L�schung des Waldbeere-Objekts
						istKollidiert = true;				
							//Update
						punkteJeRunde += 10000; 
						player1.setzePunktespielstand(punkteJeRunde);	// Punktespielstand erh�hen
						player1.zeichneSpielstandsanzeige();
							break; //Lebenszeit des Powerschusses und der Waldbeere beendet: Abbruch des Iterationsschritts 
						}
				}
				//# ENDE #f�r jeden Powerschuss auf Waldbeeren - Koordinaten �berpr�fen
				//# ----------------------------------------------------------------- #
				//# BEGINN #f�r jeden Powerschuss auf Gegner1-Koordinaten �berpr�fen, falls nicht vorher schon istKollidiert mit einer Waldbeere		
				if (!istKollidiert)
				{
					// Gegner1-Powerschuss-Kollisionspr�fung
					int g1X = (*Gegner_1.begin())->x; //x-Koordinaten eines jeden Iteratorelements: Gegner1
					int g1Y = (*Gegner_1.begin())->y; //y-Koordinaten eines jeden Iteratorelements: Gegner1
					int psX = (*it_ps)->x; //x-Koordinaten eines jeden Iteratorelements: Powersch�sse
					int psY = (*it_ps)->y; //y-Koordinaten eines jeden Iteratorelements: Powersch�sse

					if ((g1X == psX) && (g1Y == psY))
					{ //Pr�fe f�r jedes Objekt, ob es vom Powerschuss getroffen wurde, falls ja:
						CursorZurKoordinate(psX, psY); printf(" ");				// Powerschuss loeschen
						CursorZurKoordinate(g1X, g1Y); printf("0");				//1 wird 0
						Sleep(100);												//Sleep zur Visualisierung
						CursorZurKoordinate(g1X, g1Y); printf(" ");				// X weg						
							//Update
						punkteJeRunde += 100000;								
						player1.setzePunktespielstand(punkteJeRunde);	 		// Punktespielstand erh�hen -> ~10 x 100.000 = Sieg
						lebenJeRunde++;  player1.inkrHP();	    			        //Leben inkrementieren weil #Gegner zerst�rt!
						player1.zeichneSpielstandsanzeige();
							//Speicherbereinigung
						//alten Gegner 1 und Powerschuss l�schen
						delete(*it_ps);							//Zers�rung des Powerschuss-Objekts
						it_ps = Powerschuesse.erase(it_ps);		//L�schung des Powerschuss-Objekts
						delete(*Gegner_1.begin());				//Zers�rung des (einzigen) Gegner_1-Objekts
						Gegner_1.erase(Gegner_1.begin());		//L�schung des (einzigen) Gegner_1-Objekts					
						//neuen Gegner 1 erstellen
						Gegner_1.push_back(new Gegner_Level_1(meinZufallszahlengenerator(xMin, xMax), meinZufallszahlengenerator(yMin, yMax)));
						(*Gegner_1.begin())->Zeichne();
						istKollidiert = true;
						
						break; //Lebenszeit des Powerschusses und des Gegner1^beendet: Abbruch des Iterationsschritts 
					}
				}//# ENDE #f�r jeden Powerschuss auf Gegner1 - Koordinaten �berpr�fen
				 //# ----------------------------------------------------------------- #
				//Abbruchbedingung zur Speicherbereinigung: Bei Kollision nicht weiter �berpr�fen
				if (istKollidiert) break;
			}		
			//# ##### ###### ####### #//


			//2b. "EINFACHE SCHLEIFEN", if�s... - Pr�fungen der Spielfigur auf Kollision mit i) Waldbeeren, ii) Gegner1-Kontakt, iii) Gegner2-Schuss-Kontakt ...xyz... iv) Gegner2-Gegner1-Kontakt v) Spielfigur-Gegner3
			//# ++++ +++++ +++++ #//
			for (it_wb = Waldbeeren.begin(); it_wb != Waldbeeren.end(); it_wb++)
			{ //i) Spielfigur-Waldbeerenpr�fung auf Kollision
				if (player1.pruefeKollision(*it_wb))
				{	// JUHU Waldbeere aufgesammelt				
					punkteJeRunde = punkteJeRunde + 25;
					player1.setzePunktespielstand(punkteJeRunde);	//Spielstand erh�hen
					++powerJeRunde;  	player1.inkrPower();	    //Power erh�hen
				//player1.zeichneSpielstandsanzeige();			//Spielstand update
						//Update "neue" Beere: Beere woanders zeichnen
					(*it_wb)->x = meinZufallszahlengenerator(xMin, xMax);
					(*it_wb)->y = meinZufallszahlengenerator(yMin, yMax);
					(*it_wb)->Zeichne();
					 break;
				}
			}
			for (it_g1 = Gegner_1.begin(); it_g1 != Gegner_1.end(); it_g1++)
			{//ii) Spielfigur-Gegner1�berpr�fung auf Kollision
				if (player1.pruefeKollision(*it_g1))
				{
					/**///Ausnahmen f�r Pr�sentationszwecke
					 //````````````````````````````````````
						//throw exception("Hallo sehr geehrtes Publikum");					//"normale" Ausnahmebehandlung mittels 'throw'
						//throw SvensException();											//individuelle Ausnahmenbehandlung mittels 'throw'
						//throw new SvensException();//new=Exception						//Allokation mit new verursacht Systemabsturz
						//ohne throw:						
						//CursorZurKoordinate(43, 14); printf(svensStructException.what()); Sleep(5000);				//Herausschreiben einer Ausnahme ohne Systemunterbrechung
						//SvensException structExc; CursorZurKoordinate(43, 14); printf(structExc.what()); Sleep(5000); //Herausschreiben einer Ausnahme ohne Systemunterbrechung
					/**/
					
						//Update
					punkteJeRunde = punkteJeRunde - 50000;
					player1.setzePunktespielstand(punkteJeRunde);	//Spielstand reduzieren
					player1.setzeHP(--lebenJeRunde);				//Leben um 1 reduzieren weil #Gegner1!
						//Speicherbereinigung
					//alten Gegner l�schen
					delete(*it_g1);						//Zers�rung des Gegner_1-Objekts
					it_g1 = Gegner_1.erase(it_g1);		//L�schung des Gegner_1-Objekts					
					//neuen Gegner 1 erstellen
					Gegner_1.push_back(new Gegner_Level_1(meinZufallszahlengenerator(xMin, xMax), meinZufallszahlengenerator(yMin, yMax)));
					(*Gegner_1.begin())->Zeichne();
					break;
					
				}
			}
			for (it_gs = Gegner2Schuesse.begin(); it_gs != Gegner2Schuesse.end(); it_gs++)
			{//iii) 
				if (player1.pruefeKollision(*it_gs))
				{
					//Speicherbereinigung
					//Gegnerschuss-Iteratorobjekt der Liste l�schen
					delete(*it_gs);							//Zers�rung des "Gegnerschuss"-Objekts
					it_gs = Gegner2Schuesse.erase(it_gs);	//L�schung des "Gegnerschuss"-Objekts
															//Update
					punkteJeRunde = punkteJeRunde - 10000;
					player1.setzePunktespielstand(punkteJeRunde);	//Spielstand reduzieren
					--lebenJeRunde; --lebenJeRunde;
					player1.setzeHP(lebenJeRunde);				//doppelt Leben reduzieren weil #Gegner 2!

					break; //Abbruch bei Treffer
				}
				//x) Pruefe au�erdem: Gegner1 und Gegner2schuss auf Kollision�berpr�fen, (ob evtl der Gegner1 getroffen wurde, wenn ja einfach neu zeichnen)
				if ((*Gegner_1.begin())->x == ((*it_gs)->x), (*Gegner_1.begin())->y == ((*it_gs)->y))  //(it.x < this->x + 5)) && (((it.y > this->y)
				{
					(*Gegner_1.begin())->Zeichne();
				}	
				//y) for-Schleife f�r Waldbeeren aber optional -> Spiellogik gef�llt uns so...//not todo - o.k.
				//z) Speicherbereinigung: Gegner2Sch�sse "am Boden" wieder freigeben
				if ((((*it_gs)->y) >= yMax))
				{
					(*it_gs)->GegnerMove();					//Gegner2schuss loeschen
					delete(*it_gs);							//Zers�rung des Gegner2schuss-Objekts
					it_gs = Gegner2Schuesse.erase(it_gs);	//L�schung des Gegner2schuss-Objekts
					break;
				}

			}
			//iv).. Gegnerkontakt 2-1 und 3-1 links/rechts
			if (((*Gegner_2.begin())->x == (*Gegner_1.begin())->x) && (((*Gegner_2.begin())->y) == (*Gegner_1.begin())->y))  //(it.x < this->x + 5)) && (((it.y > this->y)
			{ //Pruefe Gegner1 und Gegner2  auf Kollision, (ob evtl der Gegner1 von Gegner2 �berfahren wurde, wenn ja einfach neu zeichnen)
				(*Gegner_1.begin())->Zeichne();
			}
			if (((*Gegner_3links.begin())->x == (*Gegner_1.begin())->x) && (((*Gegner_3links.begin())->y) == (*Gegner_1.begin())->y))
			{ // Pruefe Gegner1 und Gegner3  auf Kollision, (ob evtl der Gegner1 von Gegner3 �berfahren wurde, wenn ja einfach neu zeichnen)
				(*Gegner_1.begin())->Zeichne();
			}
			if (((*Gegner_3rechts.begin())->x == (*Gegner_1.begin())->x) && (((*Gegner_3rechts.begin())->y) == (*Gegner_1.begin())->y))  //(it.x < this->x + 5)) && (((it.y > this->y)
			{ // Pruefe Gegner1 und Gegner3  auf Kollision, (ob evtl der Gegner1 von Gegner3 �berfahren wurde, wenn ja einfach neu zeichnen)
				(*Gegner_1.begin())->Zeichne();
			}
			//v).. Spielfigur-Gegner3
			if ((*Gegner_3links.begin())->isAlive && player1.pruefeKollision(*Gegner_3links.begin()))
			{
					//Update
					punkteJeRunde = punkteJeRunde - 150000;
					player1.setzePunktespielstand(punkteJeRunde);	//Spielstand reduzieren
					--lebenJeRunde; --lebenJeRunde; --lebenJeRunde;	
					player1.setzeHP(lebenJeRunde);					//Leben reduzieren weil #Gegner3!
					
					(*Gegner_3links.begin())->isAlive = false;		//Gegner stirbt bei Kollision
					
			}
			if ((*Gegner_3rechts.begin())->isAlive && player1.pruefeKollision(*Gegner_3rechts.begin()))
			{
					//Update
					punkteJeRunde = punkteJeRunde - 150000;
					player1.setzePunktespielstand(punkteJeRunde);	//Spielstand reduzieren
					--lebenJeRunde; --lebenJeRunde; --lebenJeRunde;
					player1.setzeHP(lebenJeRunde);					//Leben reduzieren weil #Gegner3!

					(*Gegner_3rechts.begin())->isAlive = false;		//Gegner stirbt bei Kollision

			}		
			//# ++++ +++++ +++++ #//
			
			//######################### #2. PRUEFUNGEN  ENDE ##############################################
			//
			
			/**//**//**/
			/**//**/ // 
			/**/

			//#3. Zuletzt: Spielfigursteuerung am Ende
			player1.Move();			
		}
		//SPIELSCHLEIFE  ENDE -----------> 
		/**//**//**/
		/**//**/ 
		/**/
		if (player1.isDead())		
		{ //SIEG UND NIEDERLAGE....
			spielvorbeiNachricht();
		}
		else
		{ //...LIEGEN NAH BEIEINANDER
			spielGewonnenNachricht();
		}
		

		Sleep(25000); //damit das Fenster nicht sofort zugeht

		return 0;
	} //end try
	catch (SvensException &e)
	{//benutzerdefiniertes Exception Handling
		fuelleGesamtesSpielfeld("S");
		zeichneFensterrahmen(15, 11, 85, 17);
		CursorZurKoordinate(23, 13); printf("Ausnahmebehandlung made by Sven Kunc \n");
		CursorZurKoordinate(23, 14); cout << "SPECIFIC Exception Handling - Nachricht: " << endl;
		CursorZurKoordinate(23, 15); cout << " '' " << e.what() << " '' " << endl;
		Sleep(250000);
	}
	catch (exception &e)
	{ //standardm��ige Exceptions vom Team
		fuelleGesamtesSpielfeld("0");
		zeichneFensterrahmen(15, 11, 85, 17);
		CursorZurKoordinate(23, 13); printf("Ausnahmebehandlung made by Team WASD-Shooter-Game \n");
		CursorZurKoordinate(23, 14); cout << "STANDARD Exception Handling - Nachricht: " << endl;
		CursorZurKoordinate(23, 15); cout << " '' " << e.what() << " '' " << endl;
		Sleep(250000);
	}
	
} //end main


/**/
//default nested loop for for free
/*for (int x = xMin; x <= xMax; x++)
{
for (int y = yMin; y <= yMax; y++)
{

}
}*/