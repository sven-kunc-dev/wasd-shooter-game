#pragma once

#include "Spielfigur.cpp"

#define UP    72 // arrow keys' ascii numbers
#define LEFT  75
#define RIGHT 77
#define DOWN  80

class Spielfigur
{
private:
	int x; // x coordinate
	int y; // y coordinate
	int hp; // heart points
	int energy; // energy points
	bool isDead; // is the ship dead?
public:
	int X() { return x; }
	int Y() { return y; }
	int HP() { return hp; }

	bool isDead()
	{
		DrawSpaceShipInfo(); // It's annoying to die and still see a heart on the screen
		return isDead;
	}

	Spielfigur(int _x, int _y)
	{
		x = _x;
		y = _y;
		hp = 3; // I designed the game to have 3 lifes
		energy = 5; // And 5 energy points every life
		isDead = false; // By default you are not dead
	}
	void DrawSpaceShipInfo()
	{ // Displays HP and energy points, I aligned them with the labels printed in DrawGameLimits
		zuKoordinate(5, 1); printf("     ");
		for (int i = 0; i < hp; i++)
		{
			zuKoordinate(5 + i, 1); printf("%c", 3);
		}
		zuKoordinate(23, 1); printf("     ");
		for (int i = 0; i < energy; i++)
		{
			zuKoordinate(23 + i, 1); printf("%c", 222);
		}
	}
	void Draw()
	{ // This is our spaceship
		zuKoordinate(x, y);     printf("  %c  ", 30);
		zuKoordinate(x, y + 1); printf("  %c  ", 4);
		zuKoordinate(x, y + 2); printf("%c%c%c%c%c", 17, 30, 223, 30, 16);
	}

	//void Erase()
	//{ // This was or spaceship
	//	zuKoordinate(x, y);     printf("     ");
	//	zuKoordinate(x, y + 1); printf("     ");
	//	zuKoordinate(x, y + 2); printf("     ");
	//}
	//void Damage()
	//{ // Triggered by the asteroids that hit the spaceship
	//	energy--;
	//	if (energy == 0)
	//	{
	//		Explosion();
	//	}
	//	else
	//	{
	//		Erase(); // You can omit this part, is meant to visually tell you that you were hit
	//		zuKoordinate(x, y);     printf("  *  ");
	//		zuKoordinate(x, y + 1); printf("  *  ");
	//		zuKoordinate(x, y + 2); printf("*****");
	//		Sleep(100);
	//	}
	//}
	//void Explosion()
	//{ // When you lose a heart :c
	//	hp--;
	//	Erase();
	//	zuKoordinate(x, y);     printf("  *  ");
	//	zuKoordinate(x, y + 1); printf("  *  ");
	//	zuKoordinate(x, y + 2); printf("*****");
	//	Sleep(100);
	//	Erase();
	//	zuKoordinate(x, y);     printf(" * * ");
	//	zuKoordinate(x, y + 1); printf("* * *");
	//	zuKoordinate(x, y + 2); printf(" * * ");
	//	Sleep(100);
	//	Erase();
	//	gotoxy(x, y);     printf("*   *");
	//	gotoxy(x, y + 1); printf(" * * ");
	//	gotoxy(x, y + 2); printf("* * *");
	//	Sleep(100);
	//	if (hp > 0)
	//	{ // If you still have a heart or more
	//		energy = 5;
	//	}
	//	else
	//	{ // If you don't
	//		imDead = true;
	//	}
	//}

	void Move()
	{ // The main function of the spaceship
		if (_kbhit())
		{ // If you move the spaceship

			//Erase(); // Look I'm invisible "!!!

			char key = _getch(); // What did you type?
			switch (key)
			{ // Checks if the spaceship won't leave the game boundaries
			case LEFT:  if (x > 2)      x -= 1; break;
			case RIGHT: if (x + 4 < 77) x += 1; break;
			case UP:    if (y > 3)      y -= 1; break;
			case DOWN:  if (y + 2 < 22) y += 1; break;
			}
		}
		Draw(); // The spaceship is drawn regardless if you moved it or not, if you did then it will appear in it's new position.
	}

	~Spielfigur();
};

