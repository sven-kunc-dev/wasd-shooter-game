#pragma once

#ifndef Gegner_Level_3_H
#define Gegner_Level_3_H


class Gegner_Level_3 {


public:
	int x;
	int y;
	bool isAlive;

	Gegner_Level_3(int, int);

	void CursorZurKoordinate(int x, int y);
	void Zeichne();
	void Loesche();
	void MoveUp();
	void MoveDown();

};

#endif

