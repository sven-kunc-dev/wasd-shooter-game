#include "stdafx.h"
#include "Powerschuss.h"

#include <stdlib.h> //Standard c und c++ Bibliotheken
#include <conio.h> //Cursor platzieren - Der Name conio kommt von CONsole Input/Output.
#include <stdio.h> // Ein-/Ausgabe 1
#include <iostream>// Ein-/Ausgabe 2
#include <windows.h> //zur Kontrolle �ber das Terminal

Powerschuss::Powerschuss() { }

Powerschuss::Powerschuss(int _x, int _y)
{
	x = _x;
	y = _y;
	isAlive = true;
}


void Powerschuss::CursorZurKoordinate(int x, int y) {
	//zum "Umherlaufen" innerhalb des Controlterminals
	HANDLE hCon = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD dwPos;
	dwPos.X = x; // startet bei 0
	dwPos.Y = y; // startet bei 0
	SetConsoleCursorPosition(hCon, dwPos);
}


bool Powerschuss::isOut()
{
	if (y <= 3)
	{ // Falls Spielfeldgrenze erreicht
		CursorZurKoordinate(x, y); printf(" "); //Verschwinden des Schusses
		return true; 
	}
	else
	{
		return false;
	}
}
void Powerschuss::Move()
{
	CursorZurKoordinate(x, y); printf(" "); //alte Position loeschen
	if (y > 6)	//Spielfeldobergrenze
	{
		y--;	//"Schussbewegung des Spielers nach oben"
		CursorZurKoordinate(x, y); printf("o"); // neue Position schreiben
	}
}
void Powerschuss::GegnerMove()
{
	CursorZurKoordinate(x, y); printf(" "); //alte Position loeschen
	if (y < 24)	//Spielfelduntergrenze
	{
		y++;	//"Schussbewegung des Gegners Nr. 2 nach unten"
		CursorZurKoordinate(x, y); printf("�"); // neue Position schreiben //printf("�"); rendert leider nicht richtig :(
	}
}



