#pragma once

#ifndef Gegner_Level_2_H
#define Gegner_Level_2_H


class Gegner_Level_2 {


public:
	int x;
	int y;

	Gegner_Level_2(int, int);

	void CursorZurKoordinate(int x, int y);
	void Zeichne();
	void Loesche();
	void MoveLeft();
	void MoveRight();
};

#endif

